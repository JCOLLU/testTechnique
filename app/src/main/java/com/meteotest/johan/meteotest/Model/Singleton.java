package com.meteotest.johan.meteotest.Model;

public class Singleton {

    private City paris;
    private City london;
    private City berlin;

    private static Singleton ourInstance = new Singleton();
    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton(){}

    public City getParis() {
        return paris;
    }

    public void setParis(City paris) {
        this.paris = paris;
    }

    public City getLondon() {
        return london;
    }

    public void setLondon(City london) {
        this.london = london;
    }

    public City getBerlin() {
        return berlin;
    }

    public void setBerlin(City berlin) {
        this.berlin = berlin;
    }
}