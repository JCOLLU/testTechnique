package com.meteotest.johan.meteotest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.meteotest.johan.meteotest.Model.Singleton;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Singleton singleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Récupération Singleton
        singleton = Singleton.getInstance();

        // Ajout listener sur les boutons
        findViewById(R.id.buttonParis).setOnClickListener(this);
        findViewById(R.id.buttonLondon).setOnClickListener(this);
        findViewById(R.id.buttonBerlin).setOnClickListener(this);

        // Récupération potentiel deepLink
        String extra = getIntent().getStringExtra("city");;
        if (extra != null) {
            Intent i = new Intent(this, ListActivity.class);
            i.putExtra("city", extra);
            startActivity(i);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonParis:
                if (singleton.getParis() != null) {
                    Intent i = new Intent(MainActivity.this, ListActivity.class);
                    i.putExtra("city", "Paris");
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "Une erreur lors du chargement des données pour cette ville à eu lieu", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.buttonLondon:
                if (singleton.getLondon() != null) {
                    Intent i = new Intent(MainActivity.this, ListActivity.class);
                    i.putExtra("city", "London");
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "Une erreur lors du chargement des données pour cette ville à eu lieu", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.buttonBerlin:
                if (singleton.getBerlin() != null) {
                    Intent i = new Intent(MainActivity.this, ListActivity.class);
                    i.putExtra("city", "Berlin");
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "Une erreur lors du chargement des données pour cette ville à eu lieu", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
