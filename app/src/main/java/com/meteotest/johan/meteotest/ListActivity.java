package com.meteotest.johan.meteotest;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.meteotest.johan.meteotest.Model.City;
import com.meteotest.johan.meteotest.Model.Day;
import com.meteotest.johan.meteotest.Model.Singleton;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ListActivity extends AppCompatActivity {

    private City city;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Singleton singleton = Singleton.getInstance();
        ListView list = findViewById(R.id.listView);
        DateFormat df = new SimpleDateFormat("dd/MM", Locale.FRENCH);
        DecimalFormat formatter = new DecimalFormat("#0.0");

        // Récupération de la ville choisie
        String cityString = getIntent().getStringExtra("city");
        setTitle(cityString);
        switch (cityString) {
            case "London":
                city = singleton.getLondon();
                break;
            case "Berlin":
                city = singleton.getBerlin();
                break;
            default :
                city = singleton.getParis();
                break;
        }

        // Ajout des données à l'adapter custom
        ArrayList<HashMap<String, String>> listDays = new ArrayList<>();
        HashMap<String, String> mapDays;
        for (Day day: city.getDays()) {
            mapDays = new HashMap<>();
            mapDays.put("day", df.format(day.getDate()));
            mapDays.put("temperature", formatter.format(day.getTemperatureCelsius()) + "°C");
            mapDays.put("humidity", String.valueOf(day.getHumidity()) + "%");
            listDays.add(mapDays);
        }

        CustomAdapter adapter = new CustomAdapter(getApplicationContext(), listDays, R.layout.list_content,
                new String[]{"day", "temperature", "humidity"}, new int[]{R.id.textViewDay, R.id.textViewTemperature, R.id.textViewHumidity});
        list.setAdapter(adapter);
    }

    class CustomAdapter extends SimpleAdapter {
        private LayoutInflater layout;

        CustomAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            this.layout = LayoutInflater.from(context);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = layout.inflate(R.layout.list_content, null);
            switch (city.getDays().get(position).getWeather()) {
                case "Clouds":
                    ((ImageView) convertView.findViewById(R.id.imageView)).setImageResource(R.drawable.clouds);
                    break;
                case "Clear":
                    ((ImageView) convertView.findViewById(R.id.imageView)).setImageResource(R.drawable.sun);
                    break;
                case "Rain":
                    ((ImageView) convertView.findViewById(R.id.imageView)).setImageResource(R.drawable.rain);
                    break;
                default:
                    ((ImageView) convertView.findViewById(R.id.imageView)).setImageResource(R.drawable.question);
                    break;
            }

            return super.getView(position, convertView, parent);
        }
    }
}
