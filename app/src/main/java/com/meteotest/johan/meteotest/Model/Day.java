package com.meteotest.johan.meteotest.Model;

import java.util.Date;

public class Day {

    private Date date;
    private double temperature;
    private int humidity;
    private String weather;

    public Day() {}

    public Day(Date date, double temperature, int humidity, String weather) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.weather = weather;
    }

    public double getTemperature() {
        return temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public double getTemperatureCelsius() {
        return temperature - 273.15;
    }

    public Date getDate() {
        return date;
    }

    public String getWeather() {
        return weather;
    }
}
