package com.meteotest.johan.meteotest.Request;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
import com.octo.android.robospice.GoogleHttpClientSpiceService;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

class MyRequest<T> extends GoogleHttpClientSpiceRequest<T> {

    String baseUrl;
    boolean online = false;

    public MyRequest(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        HttpRequestFactory rf = GoogleHttpClientSpiceService.createRequestFactory();
        HttpRequest request = rf.buildGetRequest(new GenericUrl(baseUrl));
        request.setParser(new JacksonFactory().createJsonObjectParser());
        HttpResponse response = request.execute();

        return (T) response.parseAsString();
    }

    public Integer getCacheDuration() {
        if (online) {
            return Long.valueOf(DurationInMillis.ONE_HOUR).intValue();
        } else {
            return Long.valueOf(DurationInMillis.ALWAYS_RETURNED).intValue();
        }
    }
}