package com.meteotest.johan.meteotest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.meteotest.johan.meteotest.Model.City;
import com.meteotest.johan.meteotest.Model.Day;
import com.meteotest.johan.meteotest.Model.Singleton;
import com.meteotest.johan.meteotest.Request.SpiceRequestGet;
import com.octo.android.robospice.JacksonGoogleHttpClientSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SplashScreen extends AppCompatActivity {

    private SpiceManager spiceManager = new SpiceManager(JacksonGoogleHttpClientSpiceService.class);
    private ProgressDialog progressDialog;

    private Singleton singleton;
    private Uri data;
    private int loadFinish = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        // Récupération Singleton
        singleton = Singleton.getInstance();

        // Téléchargement données
        downLoad();
    }

    @Override
    protected void onStart() {
        spiceManager.start(this);

        // Récupération donnée DeepLinking
        Intent in = getIntent();
        data = in.getData();
        super.onStart();
    }


    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    public void downLoad(){
        progressDialog = new ProgressDialog(this, R.style.myTheme);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        // Récupération de l'état de la connexion internet
        boolean isOnline = isOnline();

        // Paris 6455259
        // London 2643743
        // Berlin 2950159

        // Lancement du téléchargement des données en parallèle
        String parisUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?id=6455259&cnt=14&appid=2aaf363403060b4bc1dce00e333337d4";
        SpiceRequestGet spiceRequestParis = new SpiceRequestGet(parisUrl, isOnline);
        spiceManager.execute(
                spiceRequestParis,
                "Paris",
                spiceRequestParis.getCacheDuration(),
                new SpiceRequestDataListener("Paris")
        );

        String LondonUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?id=2643743&cnt=14&appid=2aaf363403060b4bc1dce00e333337d4";
        SpiceRequestGet spiceRequestLondon = new SpiceRequestGet(LondonUrl, isOnline);
        spiceManager.execute(
                spiceRequestLondon,
                "London",
                spiceRequestLondon.getCacheDuration(),
                new SpiceRequestDataListener("London")
        );

        String BerlinUrl = "http://api.openweathermap.org/data/2.5/forecast/daily?id=2950159&cnt=14&appid=2aaf363403060b4bc1dce00e333337d4";
        SpiceRequestGet spiceRequestBerlin = new SpiceRequestGet(BerlinUrl, isOnline);
        spiceManager.execute(
                spiceRequestBerlin,
                "Berlin",
                spiceRequestBerlin.getCacheDuration(),
                new SpiceRequestDataListener("Berlin")
        );
    }

    // Vérification connexion internet
    public boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm != null ? cm.getActiveNetworkInfo() : null;
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private class SpiceRequestDataListener implements RequestListener<String> {

        private String cityName;

        SpiceRequestDataListener(String cityName) {
            this.cityName = cityName;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getApplicationContext(), "Fail download", Toast.LENGTH_SHORT).show();

            if (loadFinish < 2) {
                loadFinish++;
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }

        @Override
        public void onRequestSuccess(String result) {
            City city = parseCity(result);
            switch (cityName) {
                case "Paris":
                    singleton.setParis(city);
                    break;
                case "London":
                    singleton.setLondon(city);
                    break;
                case "Berlin":
                    singleton.setBerlin(city);
                    break;
            }

            if (loadFinish < 2) {
                loadFinish++;
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                // Gestion éventuel deepLink
                if (data != null) {
                    switch (data.toString()) {
                        case "example://Paris":
                            i.putExtra("city", "Paris");
                            break;
                        case "example://London":
                            i.putExtra("city", "London");
                            break;
                        case "example://Berlin":
                            i.putExtra("city", "Berlin");
                            break;
                    }
                }
                startActivity(i);
                finish();
            }
        }
    }

    City parseCity(String json) {
        City resultat = null;

        try {
            JSONObject res = new JSONObject(json);

            int code = res.getInt("cod");

            // Vérification de la réussite
            if (code == 200) {

                // Récupération des données de la ville
                JSONObject city = res.getJSONObject("city");
                long id = city.getLong("id");
                String name = city.getString("name");

                JSONArray array = res.getJSONArray("list");
                JSONObject obj, cityObj, weatherObj;
                Long timeStamp;
                Date date;
                Double temperature;
                int humidity;
                String weather;
                ArrayList<Day> days = new ArrayList<>();

                // Si la liste n'est pas null on boucle dessus pour récupérer les données des prochains jours
                if (array != null) {
                    for (int i = 0; i < array.length(); i++) {

                        try {
                            obj = array.getJSONObject(i);
                            cityObj = obj.getJSONObject("temp");
                            weatherObj = obj.getJSONArray("weather").getJSONObject(0);

                            // Récupération date via timeStamp
                            timeStamp = obj.getLong("dt") * 1000;
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(timeStamp);
                            date = new Date(timeStamp);

                            // Récupération température, humidité et météo
                            temperature = cityObj.getDouble("day");
                            humidity = obj.getInt("humidity");
                            weather = weatherObj.getString("main");

                            days.add(new Day(date, temperature, humidity, weather));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            days.add(new Day());
                        }

                    }
                }

                // On instancie la ville et on ajout les données des jours
                resultat = new City(id, name);
                resultat.setDays(days);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resultat;
    }

}
