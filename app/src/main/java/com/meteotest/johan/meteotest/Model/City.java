package com.meteotest.johan.meteotest.Model;

import java.util.ArrayList;

public class City {

    private long id;
    private String name;
    private ArrayList<Day> days;

    public City(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Day> getDays() {
        return days;
    }

    public void setDays(ArrayList<Day> days) {
        this.days = days;
    }
}
