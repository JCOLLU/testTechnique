package com.meteotest.johan.meteotest.Request;

public class SpiceRequestGet extends MyRequest<String> {

    public SpiceRequestGet(String baseUrl, boolean online){
        super(String.class);
        this.baseUrl = baseUrl;
        this.online = online;
    }

    @Override
    public String loadDataFromNetwork() throws Exception {
        return super.loadDataFromNetwork();
    }

}